mod openweather;
mod openweather_types;


#[cfg(test)]
mod test {
    use super::openweather_types::*;
    use super::openweather::*;

    #[tokio::test]
    async fn by_city() -> Result<(), Box<dyn std::error::Error>>  {
        let request: OpenWeatherRequest = OpenWeatherRequest{ 
            location: Location::City("Tokyo"),
            units: Units::Metric };
        let weather: OpenWeather = get(request).await?;
        println!("{:?}", weather);

        Ok(())
    }
}
