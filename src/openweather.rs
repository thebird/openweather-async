use crate::openweather_types::*;

static API_KEY: &str = "";

pub enum Location<'a> {
    City (&'a str),
    CityAndCountry (&'a str, &'a str ),
    //Coordinate { lat: f32, lon: f32 },
    //ZipCode { zip_code: &'a str, country: &'a str}
}


pub enum Units {
    Metric,
    Fahrenheit,
    Kelvin
}

impl Units {
    pub fn value(&self) -> &str {
        match *self {
            Units::Metric => "metric",
            Units::Fahrenheit => "fahrenheit",
            Units::Kelvin => "kelvin"
        }
    }
}


pub struct OpenWeatherRequest<'a> {
    pub location: Location<'a>,
    pub units: Units
}



async fn get_response(addr: &str) -> Result<String, reqwest::Error> {
    let json = reqwest::get(addr)
        .await?
        .text()
        .await?;
    Ok(json)
}

async fn parse_json(json: &str) -> Result<OpenWeather, serde_json::error::Error> {
    let data: OpenWeather = serde_json::from_str(&json.to_string())?;
    Ok(data)
}


pub async fn get(request: OpenWeatherRequest<'_>) -> Result<OpenWeather, Box<dyn std::error::Error>> {
    let base_http = "https://api.openweathermap.org/data/2.5/weather?q=";
    let addr = match request.location {
        Location::City(city) => format!(
            "{}{}&appid={}&units={}",
            &base_http,
            &city,
            API_KEY,
            request.units.value()),
        Location::CityAndCountry(city, country) => format!(
            "{}{}&appid={}&units={}",
            &base_http,
            format!("{},{}", &city, &country),
            API_KEY,
            request.units.value()),


    };


    let response = get_response(&addr).await?;
    let data = parse_json(&response).await?;
    Ok(data)
}



